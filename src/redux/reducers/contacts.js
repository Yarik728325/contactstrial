import {
  PaginationUser,
  SortName,
  currentPageActions,
  SearchContact,
  GenderFilter,
  Switcher,
  MultiSearch,
  ClearContacts,
} from '@/redux/actions/contacts';
import filterHandler from '@/utils/filter';
import Compare from '@/utils/compare';

const initialState = {
  tmp: [],
  checker: true,
  paginationUser: [],
  currentPage: 1,
  gender: '',
  search: '',
  multiselect: [],
  isCard: true,
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case ClearContacts.type:
      return {
        tmp: [],
        checker: true,
        paginationUser: [],
        currentPage: 1,
        gender: '',
        search: '',
        multiselect: [],
        isCard: true,
      };
    case SearchContact.type:
      return {
        ...state,
        search: payload,
        paginationUser: filterHandler({
          payload_: payload,
          type_: 'search',
          state,
        }),
      };
    case Switcher.type:
      return {
        ...state,
        isCard: !state.isCard,
      };
    case PaginationUser.type:
      return {
        ...state,
        checker: true,
        paginationUser: filterHandler({
          payload_: payload,
          type_: 'pagination',
          state,
        }),
        tmp: [...payload],
      };
    case MultiSearch.type:
      return {
        ...state,
        multiselect: payload,
        paginationUser: filterHandler({
          payload_: payload,
          type_: 'multi_search',
          state,
        }),
      };
    case GenderFilter.type:
      return {
        ...state,
        gender: payload,
        paginationUser: filterHandler({
          payload_: payload,
          type_: 'gender',
          state,
        }),
      };
    case currentPageActions.type:
      return {
        ...state,
        currentPage: payload,
      };
    case SortName.type:
      return {
        ...state,
        paginationUser: Compare(state),
        checker: !state.checker,
      };
    default:
      return state;
  }
};
