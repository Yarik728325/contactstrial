import {
  createAccount,
  ClearAccount,
  DatailContactAction,
} from '@/redux/actions/accounts';

const initialState = {
  currentUser: '',
  detailContact: '',
  isAuth: false,
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case createAccount.type:
      return {
        ...state,
        currentUser: payload,
        isAuth: true,
      };
    case ClearAccount.type:
      return {};
    case DatailContactAction.type:
      return {
        ...state,
        detailContact: payload,
      };
    default:
      return state;
  }
};
