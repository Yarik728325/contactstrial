import { createAction } from '@/utils/createAction';

export const PaginationUser = createAction('CONTACTS/PAGINATION');
export const SortName = createAction('/CONTACTS/SORTNAME');
export const currentPageActions = createAction('/CONTACTS/CURRENTPAGE');
export const ClearContacts = createAction('/CONTACTS/CLEARCONTACTS');
export const SearchContact = createAction('/CONTACTS/SEARCHCONTACT');
export const GenderFilter = createAction('/CONTACTS/GENDERFILTER');
export const GenderAll = createAction('/CONTACTS/GENDERALL');
export const Switcher = createAction('/CONTACTS/SWITCHER');
export const MultiSearch = createAction('CONTACTS/MULTISEARCH');
