import { createAction } from '@/utils/createAction';

export const createAccount = createAction('ACCOUNTS/CREATE');
export const ClearAccount = createAction('ACCOUNTS/CLEAACCOUNT');
export const DatailContactAction = createAction('/ACCOUNTS/DETAILCONTACT');
export const SearchContact = createAction('ACCOUNTS/SEARCHCONTACT');
