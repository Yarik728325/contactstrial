import React from 'react';
import './statistics.scss';
import countNat from '@/utils/countNat';

const Statistics = ({ StatistikCount, GenderCount }) => {
  return (
    <div className="statistics__wrapper">
      <div className="statistics">
        <div className="owner">
          <h1>Statistic</h1>
          <div className="low__wrapper">
            <div className="low__wrapper_left">
              <h2>Collection size</h2>
              <div className="statistics_count">{StatistikCount}</div>
            </div>
            <div className="low__wrapper_right">
              <div className="low__wrapper_right_wrapper">
                <div className="Males">
                  <h2>Males</h2>
                  <div className="statistics_count">
                    {countNat({
                      state: GenderCount,
                      _filter: 'male',
                      _checker: 'gender',
                    })}
                  </div>
                </div>
                <div className="Females">
                  <h2>Females</h2>
                  <div className="statistics_count">
                    {countNat({
                      state: GenderCount,
                      _filter: 'female',
                      _checker: 'gender',
                    })}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br />
      <div className="statistics__nationalities">
        <h2>Nationalities</h2>
        <div className="statistics__nationalitie_wrapper">
          <div className="statistics__card">
            <div className="statistics__card_item">
              New Zealander :
              {countNat({
                state: GenderCount,
                _filter: 'NZ',
                _checker: 'nat',
              })}
            </div>
            <div className="statistics__card_item">
              Switzerland :
              {countNat({
                state: GenderCount,
                _filter: 'CH',
                _checker: 'nat',
              })}
            </div>
            <div className="statistics__card_item">
              Irish :
              {countNat({
                state: GenderCount,
                _filter: 'IE',
                _checker: 'nat',
              })}
            </div>
          </div>
          <div className="statistics__card">
            <div className="statistics__card_item">
              Australian :
              {countNat({
                state: GenderCount,
                _filter: 'AU',
                _checker: 'nat',
              })}
            </div>
            <div className="statistics__card_item">
              Canadian :
              {countNat({
                state: GenderCount,
                _filter: 'CA',
                _checker: 'nat',
              })}
            </div>
            <div className="statistics__card_item">
              Brazilian :
              {countNat({
                state: GenderCount,
                _filter: 'BR',
                _checker: 'nat',
              })}
            </div>
          </div>
          <div className="statistics__card">
            <div className="statistics__card_item">
              British :
              {countNat({
                state: GenderCount,
                _filter: 'GB',
                _checker: 'nat',
              })}
            </div>
            <div className="statistics__card_item">
              French :
              {countNat({
                state: GenderCount,
                _filter: 'FR',
                _checker: 'nat',
              })}
            </div>
            <div className="statistics__card_item">
              German :
              {countNat({
                state: GenderCount,
                _filter: 'DE',
                _checker: 'nat',
              })}
            </div>
          </div>
          <div className="statistics__card">
            <div className="statistics__card_item">
              Spanish :
              {countNat({
                state: GenderCount,
                _filter: 'ES',
                _checker: 'nat',
              })}
            </div>
            <div className="statistics__card_item">
              Dutch :
              {countNat({
                state: GenderCount,
                _filter: 'NL',
                _checker: 'nat',
              })}
            </div>
            <div className="statistics__card_item">
              Iranian :
              {countNat({
                state: GenderCount,
                _filter: 'IR',
                _checker: 'nat',
              })}
            </div>
          </div>
          <div className="statistics__card">
            <div className="statistics__card_item">
              Finnish :
              {countNat({
                state: GenderCount,
                _filter: 'FI',
                _checker: 'nat',
              })}
            </div>
            <div className="statistics__card_item">
              Norwegian :
              {countNat({
                state: GenderCount,
                _filter: 'NO',
                _checker: 'nat',
              })}
            </div>
            <div className="statistics__card_item">
              American :
              {countNat({
                state: GenderCount,
                _filter: 'AS',
                _checker: 'nat',
              })}
            </div>
          </div>
          <div className="statistics__card">
            <div className="statistics__card_item">
              Danish :
              {countNat({
                state: GenderCount,
                _filter: 'AS',
                _checker: 'nat',
              })}
            </div>
            <div className="statistics__card_item">
              Turkish :
              {countNat({
                state: GenderCount,
                _filter: 'TR',
                _checker: 'nat',
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Statistics;
