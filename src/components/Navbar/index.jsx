import React from 'react';
import history from '@/utils/routing';
import './navbar.scss';
import { useSelector } from 'react-redux';
import DropDown from '../DropDown';

const Navbar = () => {
  const handeClick = (e, isAuth) => {
    if (isAuth) {
      return;
    }
    history.push(e);
  };
  const { isAuth } = useSelector((state) => state.account);
  console.log(isAuth);
  const mystyle = isAuth ? 'contacts' : 'anactive';
  return (
    <div className="header">
      <div className="wrapper">
        <div className="left">
          <button
            type="button"
            className="home"
            onClick={() => handeClick('/home')}
          >
            Home
          </button>
          <button
            type="button"
            className={mystyle}
            onClick={() => handeClick('/contacts')}
          >
            Contacts
          </button>
        </div>
        <button
          type="button"
          className="signIn"
          onClick={() => handeClick('/form', isAuth)}
        >
          {isAuth ? <DropDown /> : `SignIn`}
        </button>
      </div>
    </div>
  );
};

export default Navbar;
