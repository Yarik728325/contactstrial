import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import history from '@/utils/routing';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import './Contactscard.scss';
import { DatailContactAction } from '@/redux/actions/accounts';
import { useDispatch } from 'react-redux';

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
});
const Contactscard = ({
  title,
  first,
  last,
  age,
  phone,
  email,
  thumbnail,
  large,
  date,
  city,
  country,
  state,
  nat,
}) => {
  const dispatch = useDispatch();
  console.log(
    title,
    first,
    last,
    age,
    phone,
    email,
    thumbnail,
    large,
    date,
    city,
    country,
    state,
    nat,
  );
  const handeClick = (...props) => {
    dispatch(DatailContactAction(...props));
    history.push('/detail');
  };
  const classes = useStyles();
  return (
    <div className="card">
      <Card
        className={classes.root}
        onClick={() =>
          handeClick({ title, first, last, age, phone, email, large, date })
        }
      >
        <CardActionArea>
          <CardMedia
            component="img"
            alt="Contemplative Reptile"
            height="140"
            image={large}
            title="Contemplative Reptile"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {title}, {first} {last}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              <div className="contactcard">
                Email: <span>{email}</span>
              </div>
              <div className="contactcard">
                Age: <span>{age}</span>
              </div>
              <div className="contactcard">
                Phone: <span>{phone}</span>
              </div>
              <div className="contactcard">
                State: <span>{state}</span>
              </div>
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button size="small" color="primary">
            Deteil
          </Button>
        </CardActions>
      </Card>
    </div>
  );
};

export default Contactscard;
