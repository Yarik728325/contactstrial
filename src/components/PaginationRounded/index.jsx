import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';
import { useDispatch, useSelector } from 'react-redux';
import { currentPageActions } from '@/redux/actions/contacts';
import './Pagination.scss';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));

const PaginationRounded = () => {
  const { currentPage } = useSelector((state) => state.contacts);
  const dispatch = useDispatch();
  const classes = useStyles();
  const handleChange = (event, value) => {
    dispatch(currentPageActions(value));
  };
  return (
    <div className={classes.root}>
      <Pagination count={10} page={currentPage} onChange={handleChange} />
    </div>
  );
};

export default PaginationRounded;
