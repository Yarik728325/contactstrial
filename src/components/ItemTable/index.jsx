import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import './ItemTable.scss';
import history from '@/utils/routing';
import { useDispatch } from 'react-redux';
import { DatailContactAction } from '@/redux/actions/accounts';
import { Code } from '../../Constants';

const ItemTable = ({
  title,
  first,
  last,
  age,
  phone,
  email,
  thumbnail,
  large,
  date,
  city,
  country,
  state,
  nat,
}) => {
  const dispatch = useDispatch();
  const handeClick = (...props) => {
    dispatch(DatailContactAction(...props));
    history.push('/detail');
  };
  return (
    <tr className="itemtable_wrapper">
      <td className="ItemTable_item">
        <Avatar
          onClick={() =>
            handeClick({ title, first, last, age, phone, email, large, date })
          }
          alt="Remy Sharp"
          className="src"
          src={thumbnail}
        />
      </td>
      <td className="ItemTable_item">
        {title}, {first} {last}
      </td>
      <td className="ItemTable_item">{date}</td>
      <td className="ItemTable_item">{email}</td>
      <td className="ItemTable_item">{phone}7</td>
      <td className="ItemTable_item">
        /Country:{country}/ State:{state}/ City:{city}
      </td>
      <td className="ItemTable_item">
        {Code.find((x) => x.code === nat).name}
      </td>
    </tr>
  );
};

export default ItemTable;
