import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { TextField } from '@material-ui/core';
import { SearchContact } from '@/redux/actions/contacts';

const SearchPannel = () => {
  const { search } = useSelector((state) => state.contacts);
  const dispatch = useDispatch();
  const searchFilter = (e) => {
    dispatch(SearchContact(e.target.value));
  };
  return (
    <TextField
      id="standard-basic"
      label="Search by name"
      defaultValue={search || ''}
      variant="outlined"
      onChange={searchFilter}
    />
  );
};

export default SearchPannel;
