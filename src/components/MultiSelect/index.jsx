import React from 'react';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import './multiselect.scss';
import { useDispatch, useSelector } from 'react-redux';
import { MultiSearch } from '@/redux/actions/contacts';

const options = [
  { value: 'NZ', label: 'New Zealander' },
  { value: 'CH', label: 'Switzerland' },
  { value: 'AU', label: 'Australian' },
  { value: 'CA', label: 'Canadian' },
  { value: 'BR', label: 'Brazilian' },
  { value: 'GB', label: 'British' },
  { value: 'FR', label: 'French' },
  { value: 'DE', label: 'German' },
  { value: 'ES', label: 'Spanish' },
  { value: 'NL', label: 'Dutch' },
  { value: 'IR', label: 'Iranian' },
  { value: 'FI', label: 'Finnish' },
  { value: 'NO', label: 'Norwegian' },
  { value: 'US', label: 'American' },
  { value: 'DK', label: 'Danmark' },
  { value: 'TR', label: 'Turkish' },
  { value: 'IE', label: 'Irish' },
];

const animatedComponents = makeAnimated();
const MultiSelect = () => {
  const dispatch = useDispatch();
  const { multiselect } = useSelector((state) => state.contacts);
  return (
    <Select
      closeMenuOnSelect={false}
      components={animatedComponents}
      defaultValue={multiselect || ''}
      isMulti
      options={options}
      onChange={(e) => dispatch(MultiSearch(e))}
    />
  );
};

export default MultiSelect;
