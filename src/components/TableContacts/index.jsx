import React from 'react';
import { useDispatch } from 'react-redux';
import { SortName } from '@/redux/actions/contacts';

const TableContacts = ({ tmp }) => {
  const dispatch = useDispatch();
  return (
    <table className="left">
      <thead>
        <tr>
          <th>Avatar</th>
          <th
            className="fullname"
            onClick={() => {
              dispatch(SortName());
            }}
          >
            Full name
          </th>
          <th>Birthday</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Location</th>
          <th>Nationality</th>
        </tr>
      </thead>
      <tbody>{tmp}</tbody>
    </table>
  );
};
export default TableContacts;
