import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { InputLabel, FormControl, MenuItem, Select } from '@material-ui/core';
import { GenderFilter } from '@/redux/actions/contacts';

const SoloSelect = () => {
  const dispatch = useDispatch();
  const { gender } = useSelector((state) => state.contacts);
  const genderFilter = (e) => {
    dispatch(GenderFilter(e.target.value));
  };
  return (
    <FormControl>
      <InputLabel htmlFor="demo-customized-select-native" id="important" />
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        onChange={genderFilter}
        defaultValue={gender || 'all'}
      >
        <MenuItem value="all">All</MenuItem>
        <MenuItem value="male">Male</MenuItem>
        <MenuItem value="female">Female</MenuItem>
      </Select>
    </FormControl>
  );
};

export default SoloSelect;
