import React from 'react';
import { Provider } from 'react-redux';
import { Router, Route } from 'react-router-dom';
import { Home, Form, Profile, Contacts, DetailContact } from '@/pages';

import history from '@/utils/routing';
import store from '@/redux/store';
import Navbar from '../Navbar';

const App = () => {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Navbar />
        <Route exact path="/detail" component={DetailContact} />
        <Route exact path="/home" component={Home} />
        <Route exact path="/contacts" component={Contacts} />
        <Route exact path="/profile" component={Profile} />
        <Route exact path="/form" component={Form} />
      </Router>
    </Provider>
  );
};

export default App;
