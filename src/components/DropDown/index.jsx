import React from 'react';
import './dropdown.scss';
import history from '@/utils/routing';
import { Button, Menu, MenuItem } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';

import { ClearAccount } from '@/redux/actions/accounts';
import { ClearContacts } from '@/redux/actions/contacts';

const DropDown = () => {
  const dispatch = useDispatch();
  const { currentUser } = useSelector((state) => state.account);
  const { last } = currentUser.name;
  const [anchorEl, setAnchorEl] = React.useState(null);

  function handleClick(event) {
    if (anchorEl !== event.currentTarget) {
      setAnchorEl(event.currentTarget);
    }
  }

  function handleClose() {
    setAnchorEl(null);
  }
  return (
    <div>
      <div className="dropdownhello">Hello!{last}</div>
      <Button
        aria-owns={anchorEl ? 'simple-menu' : undefined}
        aria-haspopup="true"
        onClick={handleClick}
      >
        Menu
      </Button>
      <Menu
        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
        transformOrigin={{ vertical: 'top', horizontal: 'left' }}
        id="simple-menu"
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleClose}
        MenuListProps={{ onMouseLeave: handleClose }}
        getContentAnchorEl={null}
      >
        <MenuItem onClick={() => history.push('/profile')}>Profile</MenuItem>
        <MenuItem
          onClick={() => {
            dispatch(ClearAccount());
            dispatch(ClearContacts());
            history.push('/');
          }}
        >
          Logout
        </MenuItem>
      </Menu>
    </div>
  );
};
export default DropDown;
