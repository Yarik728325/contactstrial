import React from 'react';
import './form.scss';
import { Formik } from 'formik';
import history from '@/utils/routing';
import { Button } from '@material-ui/core';

const Form = () => (
  <div>
    <Formik
      initialValues={{ email: '', password: '' }}
      validate={(values) => {
        const errors = {};
        if (!values.email) {
          errors.email = 'Required';
        } else if (
          !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
          errors.email = 'Invalid email address';
        }
        return errors;
      }}
      onSubmit={() => {
        history.push('/profile');
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => (
        <form onSubmit={handleSubmit}>
          <input
            type="email"
            name="email"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.email}
          />
          {errors.email && touched.email && errors.email}
          <input
            type="password"
            name="password"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.password}
          />
          {errors.password && touched.password && errors.password}
          <Button
            variant="contained"
            color="primary"
            type="submit"
            disableElevation
            disabled={isSubmitting}
          >
            Log in
          </Button>
        </form>
      )}
    </Formik>
  </div>
);

export default Form;
