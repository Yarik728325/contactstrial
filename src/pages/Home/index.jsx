import React from 'react';
import { LogoSvg } from '@/assets/icons';
import st from './styles.module.scss';

function Home() {
  return (
    <div className={st.App}>
      <header className={st['App-header']}>
        <LogoSvg className={st['App-logo']} />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className={st['App-link']}
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default Home;
