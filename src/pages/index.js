export { default as Home } from './Home';
export { default as Form } from './Form';
export { default as Profile } from './Profile';
export { default as Contacts } from './Contacts';
export { default as DetailContact } from './DetailContact';
