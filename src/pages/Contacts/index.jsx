import ItemTable from '@/components/ItemTable';
import TableContacts from '@/components/TableContacts';
import React, { useEffect } from 'react';
import './contacts.scss';
import { PaginationUser, Switcher } from '@/redux/actions/contacts';
import { useDispatch, useSelector } from 'react-redux';
import Loading from '@/components/Loading';
import PaginationRounded from '@/components/PaginationRounded';
import axios from '@/components/api';
import Statistics from '@/components/Statistics';
import Contactscard from '@/components/Contactscard';
import Contactscards from '@/components/Contactscards';
import MultiSelect from '@/components/MultiSelect';
import SoloSelect from '@/components/SoloSelect';
import SearchPannel from '@/components/SearchPannel';
import history from '@/utils/routing';

const Contacts = () => {
  const { paginationUser, currentPage, checker, isCard } = useSelector(
    (state) => state.contacts,
  );
  const { isAuth } = useSelector((state) => state.account);
  console.log('dw');
  const dispatch = useDispatch();
  useEffect(() => {
    async function fetchDate() {
      const request = await axios.get(
        `https://randomuser.me/api/?page=${currentPage}&results=10&seed=abc`,
      );
      dispatch(PaginationUser(...request.data.results));
    }
    fetchDate();
  }, [dispatch, currentPage]);
  if (!isAuth) history.push('/form');
  if (!paginationUser) {
    return <Loading />;
  }
  const Item = isCard ? ItemTable : Contactscard;
  const Content = isCard ? TableContacts : Contactscards;
  const tmp = paginationUser.map((e) => {
    const { city, country, state } = e.location;
    const { title, first, last } = e.name;
    const { date, age } = e.dob;
    const { phone, email, nat } = e;
    const { thumbnail, large } = e.picture;
    return (
      <Item
        key={Math.random().toString(36).substr(2, 5)}
        title={title}
        first={first}
        last={last}
        large={large}
        date={date}
        age={age}
        phone={phone}
        email={email}
        thumbnail={thumbnail}
        city={city}
        country={country}
        state={state}
        nat={nat}
      />
    );
  });
  return (
    <>
      <div className="contacts_wrapper">
        <div className="contactsWrapper">
          <SearchPannel />
          <MultiSelect />
          <SoloSelect />
        </div>
        <button
          className="contactsSwitcher"
          type="button"
          onClick={() => dispatch(Switcher())}
        >
          Cards/Table
        </button>
        <Content tmp={tmp} checker={checker} />
        <div className="right">
          <PaginationRounded />
        </div>
      </div>
      <Statistics
        StatistikCount={paginationUser.length}
        GenderCount={paginationUser}
      />
    </>
  );
};

export default Contacts;
