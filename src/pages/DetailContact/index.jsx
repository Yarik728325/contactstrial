import React from 'react';
import { useSelector } from 'react-redux';

const DetailContact = () => {
  const { detailContact } = useSelector((state) => state.account);
  const { title, first, last, age, large } = detailContact;
  return (
    <div className="profile">
      <h1 className="title">Contact View</h1>
      <div className="profile_wrapper">
        <div className="profile_left">
          <img src={large} alt="" />
        </div>
        <div className="profile_right">
          <div className="profile_name">
            <span>
              {title}. {first} {last}
            </span>
            <span className="profile_years">({age} years)</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailContact;
