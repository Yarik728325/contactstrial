import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createAccount } from '@/redux/actions/accounts';
import Loading from '@/components/Loading';
import './profile.scss';
import axios from '@/components/api';

const Profile = () => {
  const dispatch = useDispatch();
  const { isAuth, currentUser } = useSelector((state) => state.account);
  useEffect(() => {
    async function fetchDate() {
      const request = await axios.get('https://randomuser.me/api/');
      dispatch(createAccount(...request.data.results));
    }
    if (!isAuth) {
      fetchDate();
    }
  }, [dispatch, isAuth]);

  if (!isAuth) {
    return <Loading />;
  }
  const { title, first, last } = currentUser.name;
  const { age } = currentUser.dob;
  const { large } = currentUser.picture;
  return (
    <div className="profile">
      <h1 className="title">Profile</h1>
      <div className="profile_wrapper">
        <div className="profile_left">
          <img src={large} alt="" />
        </div>
        <div className="profile_right">
          <div className="profile_name">
            <span>
              {title}. {first} {last}
            </span>
            <span className="profile_years">({age} years)</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Profile;
